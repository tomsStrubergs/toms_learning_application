class AddDescriptionCountToTrips < ActiveRecord::Migration
  def change
    add_column :trips, :description_count, :integer
  end
end
