class AddShownToTrips < ActiveRecord::Migration
  def change
    add_column :trips, :shown, :integer
  end
end
