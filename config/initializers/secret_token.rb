# Be sure to restart your server when you modify this file.

# Your secret key is used for verifying the integrity of signed cookies.
# If you change this key, all old signed cookies will become invalid!

# Make sure the secret is at least 30 characters and all random,
# no regular words or you'll be exposed to dictionary attacks.
# You can use `rake secret` to generate a secure secret key.

# Make sure your secret_key_base is kept private
# if you're sharing your code publicly.
Wandrr::Application.config.secret_key_base = '6bc9f029294bd62a425a73ebd27b418f01d27567ddb9fca2f65e706162a111b92f9f3fc9a74c6db5cf54101f9d753b27cb6da072988599a6e246c1fa5b0eabe0'
