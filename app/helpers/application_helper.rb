module ApplicationHelper

  def sortable(column, direct = "asc", title = nil)
    title ||= column.titleize
    if(direct == "asc")
      css_class = column == Trip.sort_column(column) ? "#{Trip.sort_direction()}" : nil
      direction = column == Trip.sort_column(column) && Trip.sort_direction() == "asc" ? "desc" : "asc"
      link_to title, params.merge(:sort => column, :direction => direction, :page => nil), {:class => css_class}
    else
      css_class = column == Trip.sort_column(column) ? "#{Trip.sort_direction("asc")}" : nil
      direction = column == Trip.sort_column(column) && Trip.sort_direction("asc") == "asc" ? "desc" : "asc"
      link_to title, params.merge(:sort => column, :direction => direction, :page => nil), {:class => css_class}
    end
  end

end
