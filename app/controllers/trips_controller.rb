class TripsController < ApplicationController
  before_action :set_trip, only: [:show, :edit, :update, :destroy, :show]
  before_action :set_user, only: [:show, :edit, :new, :index, :create, :show]
  helper_method :sort_column, :sort_direction

  # GET /trips
  # GET /trips.json
  def index
    @trips = Trip.search(params[:search]).order(Trip.sort_column(params[:sort]) + " " + Trip.sort_direction(params[:direction])).paginate(:page => params[:page], :per_page =>5)
  end

  # GET /trips/1
  # GET /trips/1.json
  def show
    @comment = Comment.new
    @trip.increse_views
    @like = Like.find_by_user_id_and_trip_id(current_user[:id], @trip.id)
  end

  # GET /trips/new
  def new
    @trip = Trip.new
  end

  # GET /trips/1/edit
  def edit
  end

  # POST /trips
  # POST /trips.json
  def create
    @trip = @user.trips.new(trip_params)

    respond_to do |format|
      if @trip.save
        format.html { redirect_to @trip, notice: 'Trip was successfully created.' }
        format.json { render action: 'show', status: :created, location: @trip }
      else
        format.html { render action: 'new' }
        format.json { render json: @trip.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /trips/1
  # PATCH/PUT /trips/1.json
  def update
    respond_to do |format|
      if @trip.update(trip_params)
        format.html { redirect_to @trip, notice: 'Trip was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: 'edit' }
        format.json { render json: @trip.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /trips/1
  # DELETE /trips/1.json
  def destroy
    @trip.destroy
    respond_to do |format|
      format.html { redirect_to trips_url }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_trip
      @trip = Trip.find(params[:id])
    end
    def set_user
      @user = User.find(current_user[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def trip_params
      params.require(:trip).permit(:name, :description)
    end

end
