class LikesController < ApplicationController
  before_action :set_trip, only: [:index, :new, :create]
  helper_method :user_name

  def index
  end

  def edit
    @like = Likes.find(params[:id])
  end

  def new
    @like = Likes.new
  end

  def create
    @trip = Trip.find(params[:trip_id])
    @like = @trip.likes.new(likes_params)
    respond_to do |format|
      if @like.save
        format.html { redirect_to @trip}
        format.json { render action: 'show', status: :created, location: @trip }
      else
        format.html { render action: 'new' }
        format.json { render json: @trip.errors, status: :unprocessable_entity }
      end
    end
  end

  def update
    respond_to do |format|
      @like = Like.find(params[:id])
      if @like.update(likes_params)
        format.html { redirect_to trip_path(@like.trip_id)}
        format.json { head :no_content }
      else
        format.html { render action: 'edit' }
        format.json { render json: @like.errors, status: :unprocessable_entity }
      end
    end
  end

  private

  def likes_params
    params.require(:likes).permit(:user_id, :like)
  end

  def set_trip
    @trip = Trip.find(params[:trip_id])
  end

end
