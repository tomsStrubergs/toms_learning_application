class CommentsController < ApplicationController
  before_action :set_comment, only: [:destroy]

  def new
    @trip = Trip.find(params[:trip_id])
    @comment = Comment.new
  end

  def create
    @trip = Trip.find(params[:trip_id])
    @comment = @trip.comments.new(comment_params)
    respond_to do |format|
      if @comment.save
        format.html { redirect_to @trip, notice: 'Comment was successfuly added' }
        format.json { render action: 'show', status: :created, location: @trip }
      else
        format.html { render action: 'new' }
        format.json { render json: @trip.errors, status: :unprocessable_entity }
      end
    end
  end
  def destroy
    @comment.destroy
    respond_to do |format|
      format.html { redirect_to trip_path(@comment.trip_id)}
      format.json { head :no_content }
    end
  end

  private
  def comment_params
    params.require(:comment).permit(:post, :author_id)
  end
  def set_comment
    @comment = Comment.find(params[:id])
  end

end
