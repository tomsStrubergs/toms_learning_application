class DashboardController < ApplicationController
  before_action :set_user, :only => [:show, :edit, :update]

  def index
  end

  def show
  end

  def edit
  end

  def update
    respond_to do |format|
      if @user.update(user_params)
        format.html { redirect_to dashboard_path(@user), notice: 'Profile was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: 'edit' }
        format.json { render json: @trip.errors, status: :unprocessable_entity }
      end
    end
  end

  private

  def user_params
    params.require(:user).permit(:name, :surname, :location, :email)
  end

  def set_user
    @user = User.find(params[:id])
  end

end
