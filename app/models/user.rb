class User < ActiveRecord::Base
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  has_many :trips, :dependent => :destroy
  has_many :comments
  has_many :likes
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatables

  def full_name
    if(name.nil? || surname.nil?)
      return email
    else
      return "#{name} #{surname}"
    end
  end

end
