class Trip < ActiveRecord::Base
  belongs_to :user
  has_many :destinations, :dependent => :destroy
  has_many :comments, :dependent => :destroy
  has_many :likes, :dependent => :destroy

  validates :name, :description, presence: true
  validates :description, length: {maximum: 200}

  scope :search, ->(search) {where('name LIKE ? OR description like ?', "%#{search}%", "%#{search}%")}

  def self.sort_column(sort)
    column_names.include?(sort) ? sort : "name"
  end

  def self.sort_direction(direction = "desc")
    %w[asc desc].include?(direction) ? direction : "asc"
  end

  def likes_count
    likes.likes.count
  end

  def dislikes_count
    likes.dislikes.count
  end

  def increse_views
    self.shown  += 1
    self.save
  end

  def count_dest
    self.destinations.count
  end

end
