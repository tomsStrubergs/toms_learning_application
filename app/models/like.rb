class Like < ActiveRecord::Base
  belongs_to :trip
  belongs_to :user

  scope :likes, -> { where(like: true)}
  scope :dislikes, -> { where(like: false)}


end
