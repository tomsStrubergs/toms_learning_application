class Destination < ActiveRecord::Base
  belongs_to :trip
  validates :name, :description, presence: true
  validates :description, length: {maximum: 200}

  def get_trip_name
    Trip.find(self.trip_id).name
  end

end
