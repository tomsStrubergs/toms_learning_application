class Comment < ActiveRecord::Base
  belongs_to :trip
  belongs_to :author, class_name: "User", foreign_key: "author_id"
  validates :post, presence: true

end
