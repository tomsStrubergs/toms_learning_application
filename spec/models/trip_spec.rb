require 'spec_helper'

describe Trip do

  describe "validations" do

    let(:trip){ Trip.new }

    context "when new record" do
      it { should have(1).error_on(:name) }
      it { should have(1).error_on(:description) }
    end

    it "fails validation with empty name" do
      trip.name = ""
      expect(trip).to have(1).error_on(:name)
    end

    it "does not fail validation with entered name" do
      trip.name = "Paris"
      expect(trip).to have(0).error_on(:name)
    end

    it "fails validation with empty description" do
      trip.description = ""
      expect(trip).to have(1).error_on(:description)
    end

    it "fails validation symbol count over 200" do
      trip.description = "12345678901234567890123456789012
      3456789012345678901234567890123456789012345678901234
      5678901234567890123456789012345678901234567890123456
      7890123456789012345678901234567890123456789012345678
      901234567890123456789012345678901234567890"
      expect(trip).to have(1).error_on(:description)
    end

    it "does not fail validation with entered description" do
      trip.description = "This is description"
      expect(trip).to have(0).error_on(:description)
    end
  end

  describe "model checking on factory create" do

    it "is named Paris" do
      trip = FactoryGirl.create(:trip)
      expect(trip.name).to eq("Paris")
    end
    it "is name not Paris" do
      trip = FactoryGirl.create(:trip, name: "Germany")
      expect(trip.name).not_to eq("Paris")
    end
  end

  describe "associations" do

    it "should have many likes, comments and destinations" do
      t = Trip.reflect_on_association(:likes)
      t.macro.should == :has_many
      t2 = Trip.reflect_on_association(:destinations)
      t2.macro.should == :has_many
      t3 = Trip.reflect_on_association(:comments)
      t3.macro.should == :has_many
    end
  end
end
