require 'spec_helper'

describe TripsController do
  describe "GET Index" do
      it "gets the index view" do
        @user = FactoryGirl.create(:user)
        sign_in @user
        get "index"
        response.status.should be 200
      end
    end
end
